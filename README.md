<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="index10.4.css">
    <title>Document</title>
    <style>
        /* #check {
            display: none;
        }

        #check + span {
            position: relative;
            width: 5px;
            height: 5px;
            background: #222222;
        }

        #check + span:before {
            content: '\2713';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            opacity: 0;
        }

        #check:checked + span:before {
            opacity: 1;
        } */
    </style>
</head>

<body>
    <header class="header">
        <div class="container header-container">
            <a class="header-logo">
                <img src="imgevklid/logo.png" alt="Логотип">
            </a> 
            <nav class="header-nav">
                <ul class="header-nav__list">
                    <li class="header-nav__item">
                        <a class="header-nav__link" href="">
                            О нас
                        </a>
                    </li>
                    <li class="header-nav__item">
                        <a class="header-nav__link" href="">
                            Проекты
                        </a>
                    </li>
                    <li class="header-nav__item">
                        <a class="header-nav__link" href="">
                            Этапы
                        </a>
                    </li>
                    <li class="header-nav__item">
                        <a class="header-nav__link" href="">
                            Отзывы
                        </a>
                    </li>
                    <li class="header-nav__item">
                        <a class="header-nav__link" href="">
                            Контакты
                        </a>
                    </li>
                </ul>
            </nav>
            <img class="header-search" src="imgevklid/search.svg" alt="">
        </div>
    </header>
    <main>
        <div class="container">
            <div class="main">
                <div class="main-title">
                    <h1 class="main-title-text">
                        Проектные решения<br>
                        любой сложности
                    </h1>
                </div>
                <div class="main-subtitle">
                    <h3 class="main-subtitle-text">
                        Есть над чем задуматься: базовые сценарии
                        поведения<br> пользователей и по сей день остаются
                        уделом проектантов
                    </h3>
                </div>
                <div class="main-btn">
                    <button class="main-btn-text">
                        Заказать расчет
                    </button>
                </div>
            </div>
            <img src="imgevklid/low-angle-photo-of-balconies.jpg" alt="">
            <div>
                <div class="main-aboutus">
                    <h2 class="title">
                        О нас
                    </h2>
                </div>
                <div class="main-article">
                    <p class="main-article-text">
                        Также как перспективное планирование создаёт необходимость включения в производственный план целого
                        ряда внеочередных
                        мероприятий<br> с учётом комплекса экспериментов, поражающих по своей масштабности и грандиозности. А
                        также диаграммы связей
                        могут быть описаны<br> максимально подробно. Мы вынуждены отталкиваться от того, что убеждённость
                        некоторых оппонентов требует
                        от нас анализа как<br> самодостаточных, так и внешне зависимых концептуальных решений! Следует отметить,
                        что высококачественный прототип <img class="main-article-image" src="imgevklid/i.svg" alt=""> будущего проекта<br> предопределяет высокую
                        востребованность позиций, занимаемых участниками в отношении поставленных
                        задач. Мы вынуждены отталкиваться<br> от того, что высококачественный прототип будущего проекта
                        способствует повышению качества экспериментов.
                    </p>
                </div>
            </div>
            <div class="main-info">
                <div class="main-img-text">
                    <div class="main-img-article">
                        <p class="main-img-paragraph">
                            Принимая во внимание показатели успешности,<br>
                            перспективное планирование способствует подготовке<br>
                            и реализации новых принципов.
                        </p>
                    </div>
                    <div class="main-img-btn">
                        <button class="main-img-settings">
                            Подробнее
                        </button>
                    </div>
                </div>
                <div class="main-img">
                    <img src="imgevklid/Rectangle2.png" alt="image">
                </div>  
                <div class="main-right">
                    <div class="right-top">
                        <div class="right-flex">
                            <div>
                                <img src="imgevklid/Ellipse5.png" alt="">
                            </div>
                            <div class="cube-image">
                                <img src="imgevklid/package.svg" alt="">
                            </div>
                        </div>
                        <div class="right-paragraph">                          
                            <h3 class="right-title">
                                Консультация с широким активом
                            </h3>
                            <p class="right-text">
                                А также свежий взгляд на привычные вещи -<br> безусловно
                                открывает новые горизонты для как<br> самодостаточных, так
                                и внешне зависимых<br> концептуальных решений.
                            </p>
                        </div>
                    </div>
                    <div class="right-bottom">
                        <div class="right-flex">
                            <div>
                                <img src="imgevklid/Ellipse5.png" alt="">
                            </div>
                            <div class="nut-flex">
                                <img src="imgevklid/settings.svg" alt="">
                            </div>
                        </div>
                        <div class="right-paragraph">
                            <h3 class="right-title">
                                В своём стремлении повысить
                            </h3>                       
                            <p class="right-text">
                                Качество жизни, они забывают, что сплочённость<br> команды
                                профессионалов представляет собой<br> интересный эксперимент
                                проверки прогресса<br> профессионального сообщества.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div>
                    <h2 class="title">
                        Как мы работаем
                    </h2>
                </div>
                <div>
                    <ul class="nav">
                        <li class="nav-item">
                            1 Шаг
                        </li>
                        <li class="nav-item">
                            2 Шаг
                        </li>
                        <li class="nav-item">
                            3 Шаг
                        </li>
                        <li class="nav-item">
                            4 Шаг
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-item">
                <div>
                    <h2 class="main-bottom-title">
                        Проводим консультацию
                    </h2>              
                    <p class="main-bottom-text">
                        Влечет за собой процесс внедрения и модернизации приоритизации разума над<br> эмоциями.
                        В рамках спецификации современных стандартов, некоторые особенности<br> внутренней политики
                        будут объективно рассмотрены соответствующими инстанциями. А<br> также представители современных
                        социальных резервов, инициированные<br>  исключительно синтетически, ограничены исключительно образом
                        мышления. Являясь<br> всего лишь частью общей картины, реплицированные с зарубежных источников,<br>
                        современные
                        исследования подвергнуты целой серии независимых исследований.<br> Кстати, стремящиеся вытеснить
                        традиционное
                        производство, нанотехнологии освещают<br> чрезвычайно интересные особенности картины в целом, однако
                        конкретные выводы,<br>
                        разумеется, призваны к ответу.
                    </p>
                    <div class="main-bottom-btn">
                        <button class="button-orange">
                            Подробнее
                        </button>
                        <button class="button-white">
                            Договор
                        </button>               
                    </div>
                </div>
                <div class="main-bottom-img">
                    <img src="imgevklid/Rectangle18.png" alt="">
                </div>
            </div>
            <div>
                <div class="title">
                    Часто задаваемые вопросы
                </div>
                <div>
                    <div class="main-list">
                        <div class="main-list-flex">
                            Из чего формируется конечная стоимость проекта?
                            <img src="imgevklid/plus.svg" alt="">
                        </div>
                    </div>
                    <div class="main-list">
                        <div class="main-list-flex">
                            У меня есть свой проект. Сможем ли мы его доработать / реализовать?
                            <img src="imgevklid/plus.svg" alt="">
                        </div>
                    </div>
                    <div class="main-list">
                        <div class="main-list-flex">
                            Я выбираю между разными компаниями. В чём ваше отличие?
                            <img src="imgevklid/plus.svg" alt="">
                        </div>
                    </div>
                    <div class="main-list">
                        <div class="main-list-flex">
                            Могу ли я делегировать вам работу / согласование с подрядчиком / организацией?
                            <img src="imgevklid/plus.svg" alt="">
                        </div>                     
                    </div>
                    <div class="main-list">
                        <div class="main-list-flex">
                            Могу ли я вернуть деньги на каком-либо из этапов работ?
                            <img src="imgevklid/plus.svg" alt="">
                        </div>                      
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="container background-dye">
            <div>
                <div class="footer-icon">
                    <img src="imgevklid/logowhite.png" alt="">
                </div>
                <div class="footer-nav">
                    <ul>
                        <li>
                            <a class="footer-nav-link" href="">
                                О нас
                            </a>
                        </li>
                        <li>
                            <a class="footer-nav-link" href="">
                                Проэкты
                            </a>
                        </li>
                        <li>
                            <a class="footer-nav-link" href="">
                                Отзывы
                            </a>
                        </li>
                        <li>
                            <a class="footer-nav-link" href="">
                                Договор оферты
                            </a>
                        </li>
                        <li>
                            <a class="footer-nav-link" href="">
                                Договор подряда
                            </a>
                        </li>
                        <li>
                            <a class="footer-nav-link" href="">
                                Конфиденциальность
                            </a>
                        </li>
                        <li>
                            <a class="footer-nav-link" href="">
                                Партнерское соглашение
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="footer-icons-flex">
                        <li class="footer-nav-item">
                            <a href="">
                                <img src="imgevklid/facebook.svg" alt="">
                            </a>
                        </li>
                        <li class="footer-nav-item">
                            <a href="">
                                <img src="imgevklid/vk.svg" alt="">
                            </a>
                        </li>
                        <li class="footer-nav-item">
                            <a href="">
                                <img src="imgevklid/instagram.svg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer-form">
                <h2 class="footer-title-form">
                    Оставить заявку
                </h2>
                <form autocomplete action="http://jsonplaseholder.typicode.com/POST" method="POST" class="footer-section-form">
                    <input required class="footer-form-string" type="text" id="name" placeholder="Фамилия, имя и отчество*">
                    <input class="footer-form-string" type="tel" placeholder="E-mail*">
                    <input class="footer-form-string-comment" type="text" placeholder="Сообщение"> 
                </form>
                <div class="footer-btn-form">
                    <button class="footer-button-form">
                        Отправить данные
                    </button>
                    <div class="footer-data">
                        <input type="checkbox" name="check" id="check" required>
                        <a class="footer-data-text" href="">
                            Согласен на обработку данных
                        </a>
                    </div>
                </div>
            </div>
            <div class="footer-text">
                <p class="footer-paragraphe">
                    Высокий уровень вовлечения<br> представителей целевой аудитории<br> является 
                    четким доказательством<br> простого факта: разбавленное изрядной<br> долей 
                    эмпатии, рациональное мышление<br> позволяет оценить значение модели<br> развития. 
                    Таким образом, консультация с<br> широким активом обеспечивает<br> широкому кругу 
                    (специалистов) участие в<br> формировании стандартных подходов. 
                </p>
            </div>
        </div>
    </footer>
</body>

</html>
